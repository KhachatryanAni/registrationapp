package am.egs.validator;

import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserAddressValidatorTest {

    private static UserAddressValidator userAddressValidator;

    @BeforeClass
    public static void init() {
        userAddressValidator = new UserAddressValidator();
    }

    @Test
    public void dataIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("")
                .setLastName("")
                .setEmail("")
                .setPassword("")
                .setTelephone("")
                .setEducation("")
                .setAdmin(true)
                .setAddress(null)
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void firstnameIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("NPUA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void lastnameIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("")
                .setEmail("a@gmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("NPUA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();

        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void emailIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("NPUA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void passwordIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.com")
                .setPassword("")
                .setTelephone("094622344")
                .setEducation("NPUA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void telephoneIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.com")
                .setPassword("1111")
                .setTelephone("")
                .setEducation("NPUA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertTrue(userAddressValidator.isValid(user));
    }

    @Test
    public void educationIsEmpty() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertTrue(userAddressValidator.isValid(user));
    }

    @Test
    public void user() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("NUACA")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertTrue(userAddressValidator.isValid(user));
    }

    @Test
    public void emailCheck() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("@gmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void emailCheck1() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("agmail.com")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void emailCheck2() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

    @Test
    public void emailCheck3() {
        UserAddressDto user = UserAddressDto.newBuilder()
                .setFirstName("Ani")
                .setLastName("Khachatryan")
                .setEmail("a@gmail.")
                .setPassword("1111")
                .setTelephone("094622344")
                .setEducation("")
                .setAdmin(true)
                .setAddress(AddressDto.newBuilder().build())
                .build();
        assertFalse(userAddressValidator.isValid(user));
    }

}