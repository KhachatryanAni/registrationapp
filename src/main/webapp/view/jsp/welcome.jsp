<%@ page import="am.egs.dto.UserAddressDto" %>
<%@ page import="am.egs.entity.Address" %>
<%@ page import="am.egs.dto.AddressDto" %>
<%--
  Created by IntelliJ IDEA.
  User: anikh
  Date: 10/3/2019
  Time: 5:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="/view/css" var="css"/>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="John Doe">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Home</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="../images/favicon.ico"/>
    <!-- Plugin-CSS -->
    <link rel="stylesheet" type ="text/css" href="${css}/bootstrap.min.css">
    <link rel="stylesheet" type ="text/css" href="${css}/owl.carousel.min.css">
    <link rel="stylesheet" type ="text/css" href="${css}/themify-icons.css">
    <link rel="stylesheet" type ="text/css" href="${css}/magnific-popup.css">
    <link rel="stylesheet" type ="text/css" href="${css}/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" type ="text/css" href="${css}/normalize.css">
    <link rel="stylesheet" type ="text/css" href="${css}/style.css">
    <link rel="stylesheet" type ="text/css" href="${css}/responsive.css">
    <script src="../js/vendor/modernizr-2.8.3.min.js"></script>

    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target="#primary-menu">
<div class="preloader">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<!--Mainmenu-area-->
<div class="mainmenu-area" data-spy="affix" data-offset-top="100">
    <div class="container">
        <!--Logo-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-menu">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand logo">
                <h2 style="font-size: 30px">energize global services</h2>
            </a>
        </div>
        <!--Logo/-->
        <nav class="collapse navbar-collapse" id="primary-menu">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="#home-page">Home</a></li>
                <li><a href="#employees-page">Employees</a></li>
                <li><a href="#education-page">Education</a></li>
                <li><a href="#contact-page">Contact</a></li>
                <li><a href="/login" class="button">Log out</a></li>
            </ul>
        </nav>
    </div>
</div>
<!--Mainmenu-area/-->


<!--Header-area-->
<header class="header-area overlay full-height relative v-center" id="home-page">
    <div class="absolute anlge-bg"></div>
    <div class="container">
        <div class="row v-center">
            <div class="col-xs-12 col-md-7 header-text">
                <h2 style="text-transform: uppercase; font-size: 20px">welcome to your personal
                    page <span
                            style="font-size: 30px"><%UserAddressDto user = (UserAddressDto) session.getAttribute("user"); %>
                        <%=user.getFirstName()%>
                    </span>
                </h2>
                <p>Here you can see the information of registered employees of EGS</p>
            </div>
        </div>
    </div>
</header>
<!--Header-area/-->


<!--Feature-area-->
<section class="gray-bg section-padding" id="employees-page">
    <div class="container">
        <div class="row">
            <div class="" style="margin-top: 2%; text-align: center">
                <form action="/users" method="get">
                    <input type="button" class="button" value="See all employees" onclick="this.form.submit()">
                </form>
            </div>
        </div>
    </div>
</section>


<!--Feature-area/-->

<section class="gray-bg section-padding" id="education-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                <div class="page-title">
                    <h2>EDUCATION</h2>
                    <p>
                        <%=user.getEducation()%>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="footer-area relative sky-bg" id="contact-page">
    <div class="absolute footer-bg"></div>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
                    <div class="page-title">
                        <h2>Contact with you</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <address style="text-align: center">
                    <div class="side-icon-box col-sm-4">
                        <div class="side-icon">
                            <img src="../images/location-arrow.png" alt="">
                        </div>
                        <p><strong>Address: </strong>
                        <p><%AddressDto address = user.getAddressDto(); %>
                            <%=address.getCity()%>,
                            <%=address.getStreet()%>,
                            apt. <%=address.getApt()%>
                        </p></p>
                    </div>
                    <div class="side-icon-box col-sm-4">
                        <div class="side-icon">
                            <img src="../images/phone-arrow.png" alt="">
                        </div>
                        <p><strong>Telephone: </strong>
                            <%= user.getTelephone() %>
                        </p>
                    </div>
                    <div class="side-icon-box col-sm-4">
                        <div class="side-icon">
                            <img src="../images/mail-arrow.png" alt="">
                        </div>
                        <p><strong>E-mail: </strong>
                            <%= user.getEmail() %>
                        </p>
                    </div>
                </address>
            </div>
        </div>
    </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>&copy;Copyright 2019 All right resurved. This template is made with <i class="ti-heart"
                                                                                              aria-hidden="true"></i> by
                        <a href="http://energizeglobal.com/">EGS</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>


<!--Vendor-JS-->
<script src="../js/vendor/jquery-1.12.4.min.js"></script>
<script src="../js/vendor/bootstrap.min.js"></script>
<!--Plugin-JS-->
<script src="../js/owl.carousel.min.js"></script>
<script src="../js/contact-form.js"></script>
<script src="../js/jquery.parallax-1.1.3.js"></script>
<script src="../js/scrollUp.min.js"></script>
<script src="../js/magnific-popup.min.js"></script>
<script src="../js/wow.min.js"></script>
<!--Main-active-JS-->
<script src="../js/main.js"></script>
</body>

</html>
