<%@ page import="am.egs.dto.UserAddressDto" %>
<%@ page import="java.util.List" %>
<%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<%--
  Created by IntelliJ IDEA.
  User: anikh
  Date: 10/4/2019
  Time: 5:35 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employees</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <table class="table table-striped table-hover" style="width: 80%; margin: 20px auto;">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Firstname</th>
            <th scope="col">Lastname</th>
            <th scope="col">Email</th>
            <th scope="col">Education</th>
            <th scope="col">Telephone</th>
            <th scope="col">Address</th>
        </tr>
        </thead>
        <tbody>
        <%
            List<UserAddressDto> list = (List<UserAddressDto>) session.getAttribute("employees");
            for (UserAddressDto user : list) {%>
        <tr>
            <td><%=user.getFirstName()%>
            </td>
            <td><%=user.getLastName()%>
            </td>
            <td><%=user.getEmail()%>
            </td>
            <td><%=user.getEducation()%>
            </td>
            <td><%=user.getTelephone()%>
            </td>
            <td><%=user.getAddressDto()%>
            </td>
        </tr>
        <%}%>
        </tbody>
    </table>
    <a href="/login" class="button">Log out</a>
</div>
</body>
</html>