<%@ page import="am.egs.entity.User" %>
<%@ page import="am.egs.service.UserServiceImpl" %>
<%@ page import="java.util.List" %>
<%@ page import="am.egs.dto.UserAddressDto" %><%--
  Created by IntelliJ IDEA.
  User: anikh
  Date: 10/10/2019
  Time: 12:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="/view/css" var="css"/>
<!DOCTYPE html>
<html>
<head>
    <title>Employees</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" type ="text/css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type ="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="col-xs-1 col-sm-1 col-md-1 side-icon-box" style="margin-top: 35px">
        <a href="/view/jsp/welcome.jsp">Go to my page</a><hr>
        <a href="/login" class="button">Log out</a>
    </div>
    <div class="col-xs-11 col-sm-11 col-md-11">
        <table class="table table-striped table-hover" style="width: 80%; margin: 20px auto;">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Firstname</th>
                <th scope="col">Lastname</th>
                <th scope="col">Email</th>
                <th scope="col">Education</th>
                <th scope="col">Telephone</th>
                <th scope="col">Address</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
            <tbody>
            <%
                List<UserAddressDto> list = UserServiceImpl.getInstance().getAll();

                for (UserAddressDto user : list) {%>
            <tr>
                <td><%=user.getFirstName()%>
                </td>
                <td><%=user.getLastName()%>
                </td>
                <td><%=user.getEmail()%>
                </td>
                <td><%=user.getEducation()%>
                </td>
                <td><%=user.getTelephone()%>
                </td>
                <td><%=user.getAddressDto()%>
                </td>
                <td>
                    <%String email = user.getEmail();%>
                    <form action="/admin" method="post">
                        <button name="email" type="submit" value="<%=email%>" class="btn btn-info btn-block">DELETE</button></form>
                </td>
            </tr>
            <%}%>
            </tbody>
        </table>

    </div>

</div>
</body>
</html>