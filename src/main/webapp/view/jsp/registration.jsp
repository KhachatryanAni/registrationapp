<%@ page import="am.egs.enums.City" %><%--
  Created by IntelliJ IDEA.
  User: anikh
  Date: 9/30/2019
  Time: 3:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Registration</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" type="text/css" rel="stylesheet"
          id="bootstrap-css">
    <%--<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>--%>
</head>
<body>
<div class="container" style="padding-top:50px">
    <div class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Please sign up </h3>
            </div>
            <div class="panel-body" style="padding-top:30px">
                <form role="form" action="/registration" method="post">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="firstname" id="first_name" class="form-control input-sm"
                                       placeholder="First Name" required>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="lastname" id="last_name" class="form-control input-sm"
                                       placeholder="Last Name" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control input-sm"
                               placeholder="Email Address" required>
                    </div>

                    <div class="row">
                        <div class="panel-heading">
                            <fieldset class="col-md-14">
                                <legend style="font-size: 12px; ">Address</legend>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <% City[] cities = City.values(); %>
                                        <select name="city" id="city" class="form-control" style="color: gray">
                                            <%
                                                for (City city : cities) {
                                                    StringBuilder name = new StringBuilder(city.name().toLowerCase());
                                                    name.replace(0, 1, "" + city.name().charAt(0));
                                            %>
                                            <option value=<%=name%>><%=name%></option>
                                            <% }%>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="street" id="street" class="form-control input-sm"
                                               placeholder="Street" required>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <input type="number" name="apt" id="apt" class="form-control input-sm"
                                               placeholder="Apt" required>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="text" name="education" id="education" class="form-control input-sm"
                                       placeholder="Education" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="tel" name="telephone" id="telephone" class="form-control input-sm"
                                       placeholder="Telephone" required>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-sm"
                                       placeholder="Password" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-group" style="margin: 0 auto">
                            <label style="font-weight: bold">Want to register as admin? Answer the question:</label><br>
                            <span style="color: #3c763d">Give me food, and I will live; give me water, and I will die. What am I?</span><br>
                            <input id="login-admin" type="text" name="admin-question" class="form-control input-sm">

                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
                        <input type="submit" value="Register" class="btn btn-info btn-block">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
