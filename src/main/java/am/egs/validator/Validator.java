package am.egs.validator;

public interface Validator<T> {
    boolean isValid(T t);
}
