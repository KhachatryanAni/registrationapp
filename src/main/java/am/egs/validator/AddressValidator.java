package am.egs.validator;

import am.egs.dto.AddressDto;

public class AddressValidator implements Validator<AddressDto> {
    @Override
    public boolean isValid(AddressDto address) {
        if (address.getCity() != null && address.getStreet() != null && address.getApt() > 0)
            return true;
        return false;
    }
}
