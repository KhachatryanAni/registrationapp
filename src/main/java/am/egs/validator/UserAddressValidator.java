package am.egs.validator;

import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import am.egs.entity.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserAddressValidator implements Validator<UserAddressDto> {
    @Override
    public boolean isValid(UserAddressDto user) {
        if (user != null){
            String firstname = user.getFirstName();
            String lastname = user.getLastName();
            String email = user.getEmail();
            String password = user.getPassword();

            String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
            AddressDto addressDto = user.getAddressDto();
            Validator<AddressDto> addressDtoValidator = new AddressValidator();
            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            boolean matches = matcher.matches();

            if (firstname != null && lastname != null && email != null && matches && password != null && addressDtoValidator.isValid(addressDto)){
                if (!firstname.equals("") && !lastname.equals("") && !email.equals("") && !password.equals(""))
                    return true;
            }
        }
        return false;
    }
}
