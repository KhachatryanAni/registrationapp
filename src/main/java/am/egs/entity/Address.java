package am.egs.entity;

public class Address {
    private int id;
    private String city;
    private String street;
    private int apt;

    public Address() {
    }

    public Address(String city, String street, int apt) {
        this.city = city;
        this.street = street;
        this.apt = apt;
    }

    public Address(int id, String city, String street, int apt) {
        this.id = id;
        this.city = city;
        this.street = street;
        this.apt = apt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getApt() {
        return apt;
    }

    public void setApt(int apt) {
        this.apt = apt;
    }

    @Override
    public String toString() {
        return city + ", " + street + ", apt." + apt;
    }
}
