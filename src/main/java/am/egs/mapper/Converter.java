package am.egs.mapper;

import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import am.egs.entity.Address;
import am.egs.entity.User;

import java.util.ArrayList;
import java.util.List;

public class Converter {

    private static Converter instance;

    private Converter(){}

    public User mapToUser(UserDto userDto) {
        User user = new User();
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setEducation(userDto.getEducation());
        user.setTelephone(userDto.getTelephone());
        user.setAdmin(userDto.isAdmin());
        user.setAddressId(userDto.getAddressId());
        return user;
    }

    public UserDto mapToUserDto(User user) {
        return UserDto.newBuilder()
                .setFirstname(user.getFirstName())
                .setLastname(user.getLastName())
                .setEmail(user.getEmail())
                .setPassword(user.getPassword())
                .setEducation(user.getEducation())
                .setTelephone(user.getTelephone())
                .setAdmin(user.isAdmin())
                .setAddressId(user.getAddressId()).build();

    }

    public Address mapToAddress(AddressDto addressDto) {
        Address address = new Address();
        address.setCity(addressDto.getCity());
        address.setStreet(addressDto.getStreet());
        address.setApt(addressDto.getApt());
        return address;
    }

    public AddressDto mapToAddressDto(Address address) {
        return AddressDto.newBuilder()
                .setCity(address.getCity())
                .setStreet(address.getStreet())
                .setApt(address.getApt())
                .build();
    }

    public User mapToUser(UserAddressDto userAddressDto) {
        User user = new User();
        user.setFirstName(userAddressDto.getFirstName());
        user.setLastName(userAddressDto.getLastName());
        user.setEmail(userAddressDto.getEmail());
        user.setPassword(userAddressDto.getPassword());
        user.setEducation(userAddressDto.getEducation());
        user.setTelephone(userAddressDto.getTelephone());
        user.setAdmin(userAddressDto.isAdmin());
        user.setAddressId(userAddressDto.getAddressDto().getId());
        return user;
    }

    public List<UserDto> getAllUsersDto(List<User> userList){
        List<UserDto> dtoList = new ArrayList<>();
        for (User user : userList) {
            dtoList.add(mapToUserDto(user));
        }
        return dtoList;
    }

    public static Converter getInstance(){
        if (instance == null) {
            synchronized (Converter.class) {
                if (instance == null) {
                    instance = new Converter();
                }
            }
        }
        return instance;
    }
}
