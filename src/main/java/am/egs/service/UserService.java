package am.egs.service;

import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import am.egs.entity.Address;
import am.egs.entity.User;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserNotFoundException;
import am.egs.exception.UserAlreadyExistsException;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.List;

public interface UserService {
    UserAddressDto get(int id) throws UserNotFoundException, SQLException;

    UserAddressDto getByEmail(String email) throws UserNotFoundException, SQLException;

    List getAll() throws EmptyDataException, SQLException;

    int save(UserAddressDto userAddressDto) throws InvalidUserException, UserAlreadyExistsException, SQLException;

    boolean update(UserDto user, AddressDto address) throws InvalidUserException, UserNotFoundException, SQLException;

    boolean delete(String email) throws UserNotFoundException, SQLException;

    boolean login(String email, String password) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException, UserNotFoundException;
}
