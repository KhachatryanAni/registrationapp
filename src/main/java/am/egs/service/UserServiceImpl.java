package am.egs.service;

import am.egs.config.Hash;
import am.egs.connection.JdbcConnection;
import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import am.egs.entity.Address;
import am.egs.entity.User;
import am.egs.exception.EmptyDataException;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.exception.UserNotFoundException;
import am.egs.mapper.Converter;
import am.egs.repository.AddressDao;
import am.egs.repository.AddressDaoImpl;
import am.egs.repository.UserDao;
import am.egs.repository.UserDaoImpl;
import am.egs.validator.UserAddressValidator;
import am.egs.validator.Validator;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private AddressDao addressDao;
    private Validator<UserAddressDto> userAddressValidator;
    private DataSource mysqlDataSource;
    private static final Logger LOGGER = Logger.getLogger(UserServiceImpl.class);
    private static UserService instance;
    private static Converter mapper;


    private UserServiceImpl() {
        userDao = new UserDaoImpl();
        mapper = Converter.getInstance();
        addressDao = new AddressDaoImpl();
        userAddressValidator = new UserAddressValidator();
        mysqlDataSource = JdbcConnection.getInstance();
    }

    @Override
    public UserAddressDto get(int id) throws UserNotFoundException, SQLException {
        Connection connection = mysqlDataSource.getConnection();
        User userEntity = userDao.get(id, connection);
        if (userEntity != null) {
            UserDto user = mapper.mapToUserDto(userEntity);
            Address addressEntity = addressDao.get(user.getAddressId(), connection);
            AddressDto address = mapper.mapToAddressDto(addressEntity);
            return UserAddressDto.newBuilder()
                    .setUserAndAddress(user, address)
                    .build();
        }
        throw new UserNotFoundException();
    }

    @Override
    public UserAddressDto getByEmail(String email) throws UserNotFoundException, SQLException {
        Connection connection = mysqlDataSource.getConnection();
        User userByEmail = userDao.getByEmail(email, connection);

        if (userByEmail != null) {
            userByEmail.setEmail(email);
            UserDto user = mapper.mapToUserDto(userByEmail);
            Address addressEntity = addressDao.get(user.getAddressId(), connection);
            AddressDto address = mapper.mapToAddressDto(addressEntity);
            return UserAddressDto.newBuilder()
                    .setUserAndAddress(user, address)
                    .build();
        }
        throw new UserNotFoundException();
    }

    @Override
    public List<UserAddressDto> getAll() throws EmptyDataException, SQLException {
        Connection connection = mysqlDataSource.getConnection();
        List<User> userDaoAll = userDao.getAll(connection);
        List<UserAddressDto> dtoList = new ArrayList<>();
        if (userDaoAll != null) {
            List<UserDto> users = mapper.getAllUsersDto(userDaoAll);
            for (UserDto user : users) {
                Address addressEntity = addressDao.get(user.getAddressId(), connection);
                AddressDto address = mapper.mapToAddressDto(addressEntity);
                UserAddressDto userAddressDto = UserAddressDto.newBuilder()
                        .setUserAndAddress(user, address)
                        .build();
                dtoList.add(userAddressDto);
            }
            return dtoList;
        }
        throw new EmptyDataException();
    }

    @Override
    public int save(UserAddressDto userAddressDto) throws InvalidUserException, UserAlreadyExistsException, SQLException {
        Connection connection = mysqlDataSource.getConnection();
        int saved = -1;
        boolean exists = (userDao.getByEmail(userAddressDto.getEmail(), connection) != null);
        if (exists) {
            throw new UserAlreadyExistsException();
        } else if (userAddressValidator.isValid(userAddressDto)) {
            try {
                connection.setAutoCommit(false);
                AddressDto address = userAddressDto.getAddressDto();
                int byStreetAndApt = addressDao.getByCityAndStreetAndApt(address.getCity(), address.getStreet(), address.getApt(), connection);
                if (byStreetAndApt > 0) {
                    address = AddressDto.newBuilder()
                            .setId(byStreetAndApt)
                            .setCity(address.getCity())
                            .setStreet(address.getStreet())
                            .setApt(address.getApt()).build();
                    userAddressDto = UserAddressDto.newBuilder()
                            .setFirstName(userAddressDto.getFirstName())
                            .setLastName(userAddressDto.getLastName())
                            .setEmail(userAddressDto.getEmail())
                            .setPassword(userAddressDto.getPassword())
                            .setTelephone(userAddressDto.getTelephone())
                            .setEducation(userAddressDto.getEducation())
                            .setAdmin(userAddressDto.isAdmin())
                            .setAddress(address).build();

                } else {
                    int addressId = addressDao.save(mapper.mapToAddress(address), connection);
                    address = AddressDto.newBuilder()
                            .setId(addressId)
                            .setCity(address.getCity())
                            .setStreet(address.getStreet())
                            .setApt(address.getApt()).build();
                    userAddressDto = UserAddressDto.newBuilder()
                            .setFirstName(userAddressDto.getFirstName())
                            .setLastName(userAddressDto.getLastName())
                            .setEmail(userAddressDto.getEmail())
                            .setPassword(userAddressDto.getPassword())
                            .setTelephone(userAddressDto.getTelephone())
                            .setEducation(userAddressDto.getEducation())
                            .setAdmin(userAddressDto.isAdmin())
                            .setAddress(address).build();
                }

                saved = userDao.save(mapper.mapToUser(userAddressDto), connection);
                connection.commit();
                LOGGER.info("User with email " + userAddressDto.getEmail() + " has been saved");
                return saved;
            } catch (SQLException e) {
                try {
                    connection.rollback();
                    LOGGER.error("Error during saving user with email " + userAddressDto.getEmail());
                } catch (SQLException ex) {
                    LOGGER.error("Error during rollback ");
                }
            } catch (InvalidKeySpecException e) {
                //
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } finally {

                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error during connection close");
                }

            }
        } else {
            throw new InvalidUserException();
        }
        return saved;
    }

    @Override
    public boolean update(UserDto user, AddressDto address) throws UserNotFoundException, SQLException {
        Connection connection = mysqlDataSource.getConnection();

        boolean updated = false;
        boolean exists = (userDao.getByEmail(user.getEmail(), connection) != null);
        if (exists) {
            try {
                connection.setAutoCommit(false);
                addressDao.update(mapper.mapToAddress(address), connection);
                updated = userDao.update(mapper.mapToUser(user), connection);
                connection.commit();
                LOGGER.info("User with email " + user.getEmail() + "has been updated");
                return updated;
            } catch (SQLException e) {
                try {
                    connection.rollback();
                    LOGGER.error("Error during updating user with email " + user.getEmail());
                } catch (SQLException ex) {
                    LOGGER.error("Error during rollback ");
                }
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error during connection close");
                }

            }
        } else {
            throw new UserNotFoundException();
        }
        return updated;
    }

    @Override
    public boolean delete(String email) throws UserNotFoundException, SQLException {
        Connection connection = mysqlDataSource.getConnection();

        boolean deleted = false;
        User byEmail = userDao.getByEmail(email, connection);
        boolean exists = (byEmail != null);
        if (exists) {
            try {
                int id = byEmail.getId();
                connection.setAutoCommit(false);
                int addressId = userDao.get(id, connection).getAddressId();
                List<User> userList = userDao.getAllWithSameAddress(addressId, connection);
                if (userList.size() == 1) {
                    addressDao.delete(addressId, connection);
                }
                deleted = userDao.delete(id, connection);
                connection.commit();
                connection.setAutoCommit(true);
                LOGGER.info("User with id " + id + "has been deleted");
                return deleted;
            } catch (SQLException e) {
                try {
                    LOGGER.error("Error during getting users with same address " + email);
                    connection.rollback();
                } catch (SQLException ex) {
                    LOGGER.error("Error during rollback ");
                }
            } finally {
                try {
                    connection.close();
                } catch (SQLException e) {
                    LOGGER.error("Error during connection close");
                }
            }
        } else throw new UserNotFoundException();
        return deleted;
    }

    public boolean login(String email, String password) throws SQLException, UserNotFoundException {
        Connection connection = mysqlDataSource.getConnection();
        User userByEmail = userDao.getByEmail(email, connection);
        String hashedPass = null;
        if (userByEmail!=null)
            hashedPass = userByEmail.getPassword();
        else throw new UserNotFoundException();
        boolean validPassword = false;
        try {
            validPassword = Hash.validatePassword(password, hashedPass);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("No Such Algorithm");
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Invalid Key Spec");
        }
        if ((userByEmail != null) && validPassword)
            return true;
        return false;
    }

    public static UserService getInstance() {
        if (instance == null) {
            synchronized (UserServiceImpl.class) {
                if (instance == null) {
                    instance = new UserServiceImpl();
                }
            }
        }
        return instance;
    }

}
