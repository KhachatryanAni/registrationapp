package am.egs.controller;

import am.egs.dto.AddressDto;
import am.egs.dto.UserAddressDto;
import am.egs.dto.UserDto;
import am.egs.entity.Address;
import am.egs.entity.User;
import am.egs.exception.InvalidUserException;
import am.egs.exception.UserAlreadyExistsException;
import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import am.egs.service.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(RegistrationServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String education = req.getParameter("education");
        String telephone = req.getParameter("telephone");
        String city = req.getParameter("city");
        String street = req.getParameter("street");
        int apt = Integer.parseInt(req.getParameter("apt"));
        String answer = req.getParameter("admin-question");
        boolean isAdmin = false;
        if (answer.toLowerCase() != null && answer.toLowerCase().equals("fire")) {
            isAdmin = true;
        }

        UserService userService = UserServiceImpl.getInstance();

        boolean exists = true;
        try {
            userService.getByEmail(email);
        } catch (UserNotFoundException e) {
            exists = false;
        } catch (SQLException e) {
            //
        }
        try {
            if (!exists) {
                AddressDto addressDto = AddressDto.newBuilder()
                        .setCity(city)
                        .setStreet(street)
                        .setApt(apt)
                        .build();
                UserAddressDto userAddressDto = UserAddressDto.newBuilder()
                        .setFirstName(firstname)
                        .setLastName(lastname)
                        .setEmail(email)
                        .setPassword(password)
                        .setTelephone(telephone)
                        .setEducation(education)
                        .setAdmin(isAdmin)
                        .setAddress(addressDto)
                        .build();
                userService.save(userAddressDto);

                HttpSession session = req.getSession();
                session.setAttribute("user", userAddressDto);
                LOGGER.info("User successfully registered with email " + email);

                req.getRequestDispatcher("/login").forward(req, resp);

            } else throw new UserAlreadyExistsException();
        } catch (UserAlreadyExistsException e) {
            LOGGER.error("Can't registrate user. User already exists");
            resp.sendError(HttpServletResponse.SC_CONFLICT, "User already exists");
        } catch (InvalidUserException e) {
            LOGGER.error("Invalid fields for user creation");
            resp.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Invalid fields for user creation");
        } catch (SQLException e) {
            LOGGER.error("Error with database");
        } catch (ServletException e) {
            //
        }
        resp.setContentType("text/html");
    }
}
