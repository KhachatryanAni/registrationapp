package am.egs.controller;

import am.egs.dto.UserAddressDto;
import am.egs.entity.User;
import am.egs.exception.EmptyDataException;
import am.egs.service.UserService;
import am.egs.service.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/users")
public class UserServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(UserServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserService userService = UserServiceImpl.getInstance();
        try {
            List<UserAddressDto> list = userService.getAll();
            req.getSession().setAttribute("employees", list);
            resp.sendRedirect("view/jsp/employees.jsp");
        } catch (EmptyDataException e) {
            resp.sendError(HttpServletResponse.SC_NO_CONTENT, "Empty data");
        } catch (SQLException e) {
           LOGGER.error("Error with database");
        }
        resp.setContentType("text/html");
    }


}
