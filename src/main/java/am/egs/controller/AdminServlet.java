package am.egs.controller;

import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import am.egs.service.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/admin")
public class AdminServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(AdminServlet.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        UserService userService = UserServiceImpl.getInstance();

        String email = req.getParameter("email");
        try {
            userService.delete(email);
            resp.sendRedirect("/view/jsp/adminPage.jsp");
        } catch (UserNotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "No such user");
        } catch (SQLException e) {
            LOGGER.error("Error during deleting user with email " + email);
        }
        resp.setContentType("text/html");
    }
}
