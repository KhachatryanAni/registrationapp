package am.egs.controller;

import am.egs.dto.UserAddressDto;
import am.egs.exception.UserNotFoundException;
import am.egs.service.UserService;
import am.egs.service.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(LoginServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        req.getRequestDispatcher("view/jsp/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  IOException {

        String email = req.getParameter("email");
        String password = req.getParameter("password");
        UserService userService = UserServiceImpl.getInstance();
        try {
            if (userService.login(email, password)) {
                LOGGER.info("User with email " + email + " has been successfully logged in");
                HttpSession session = req.getSession();
                UserAddressDto userAddressDto = userService.getByEmail(email);
                session.setAttribute("user", userAddressDto);
                if (userAddressDto.isAdmin()) {
                    resp.sendRedirect("/view/jsp/adminPage.jsp");
                } else {
                    resp.sendRedirect("/view/jsp/welcome.jsp");
                }

            } else {
                resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Incorrect email or password");
            }
        } catch (UserNotFoundException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Incorrect email or password");
        } catch (SQLException e) {
            LOGGER.error("Cant log in with email and password " + email);
        } catch (InvalidKeySpecException e) {
            LOGGER.error("Problem with password hashing algorithm's key");
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Problem with password hashing algorithm");
        }

    }
}
