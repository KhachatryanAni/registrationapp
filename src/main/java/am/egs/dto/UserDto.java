package am.egs.dto;

import am.egs.entity.User;

public class UserDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String telephone;
    private String education;
    private boolean isAdmin;
    private int addressId;

    private UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEducation() {
        return education;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public int getAddressId() {
        return addressId;
    }

    public static UserDto.Builder newBuilder() {
        return new UserDto().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public UserDto.Builder setFirstname(String firstname) {
            UserDto.this.firstName = firstname;
            return this;
        }

        public UserDto.Builder setLastname(String lastname) {
            UserDto.this.lastName = lastname;
            return this;
        }

        public UserDto.Builder setEmail(String email) {
            UserDto.this.email = email;
            return this;
        }

        public UserDto.Builder setPassword(String password) {
            UserDto.this.password = password;
            return this;
        }

        public UserDto.Builder setTelephone(String telephone) {
            UserDto.this.telephone = telephone;
            return this;
        }

        public UserDto.Builder setEducation(String education) {
            UserDto.this.education = education;
            return this;
        }

        public UserDto.Builder setAdmin(boolean admin) {
            UserDto.this.isAdmin = admin;
            return this;
        }

        public UserDto.Builder setAddressId(int addressId) {
            UserDto.this.addressId = addressId;
            return this;
        }

        public UserDto build() {
            return UserDto.this;
        }
    }

}
