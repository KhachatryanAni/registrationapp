package am.egs.dto;

public class UserAddressDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String telephone;
    private String education;
    private boolean isAdmin;
    private AddressDto addressDto;

    private UserAddressDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getEducation() {
        return education;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public AddressDto getAddressDto() {
        return addressDto;
    }

    public static UserAddressDto.Builder newBuilder() {
        return new UserAddressDto().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public UserAddressDto.Builder setFirstName(String firstName) {
            UserAddressDto.this.firstName = firstName;
            return this;
        }

        public UserAddressDto.Builder setLastName(String lastName) {
            UserAddressDto.this.lastName = lastName;
            return this;
        }

        public UserAddressDto.Builder setEmail(String email) {
            UserAddressDto.this.email = email;
            return this;
        }

        public UserAddressDto.Builder setPassword(String password) {
            UserAddressDto.this.password = password;
            return this;
        }

        public UserAddressDto.Builder setTelephone(String telephone) {
            UserAddressDto.this.telephone = telephone;
            return this;
        }

        public UserAddressDto.Builder setEducation(String education) {
            UserAddressDto.this.education = education;
            return this;
        }

        public UserAddressDto.Builder setAdmin(boolean admin) {
            UserAddressDto.this.isAdmin = admin;
            return this;
        }

        public UserAddressDto.Builder setAddress(AddressDto addressDto) {
            UserAddressDto.this.addressDto = addressDto;
            return this;
        }

        public UserAddressDto.Builder setUserAndAddress(UserDto userDto, AddressDto addressDto) {
            UserAddressDto.this.firstName = userDto.getFirstName();
            UserAddressDto.this.lastName = userDto.getLastName();
            UserAddressDto.this.email = userDto.getEmail();
            UserAddressDto.this.education = userDto.getEducation();
            UserAddressDto.this.telephone = userDto.getTelephone();
            UserAddressDto.this.isAdmin = userDto.isAdmin();
            UserAddressDto.this.password = userDto.getPassword();
            UserAddressDto.this.addressDto = addressDto;
            return this;
        }

        public UserAddressDto build() {
            return UserAddressDto.this;
        }
    }
}
