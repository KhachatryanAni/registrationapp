package am.egs.repository;

import am.egs.entity.User;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface UserDao {
    User get(int id, Connection connection) throws SQLException;

    User getByEmail(String email, Connection connection) throws SQLException;

    List<User> getAll(Connection connection) throws SQLException;

    int save(User user, Connection connection) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException;

    boolean update(User user, Connection connection) throws SQLException;

    boolean delete(int id, Connection connection) throws SQLException;

    List<User> getAllWithSameAddress(int id, Connection connection) throws SQLException;
}
