package am.egs.repository;

import am.egs.entity.Address;

import java.sql.*;

public class AddressDaoImpl implements AddressDao {

    private static final String INSERT_ADDRESS_SQL = "INSERT INTO address (city, street, apt) VALUES (?, ?, ?);";
    private static final String SELECT_ADDRESS_BY_ID = "SELECT * FROM address WHERE id = ?";
    private static final String SELECT_BY_CITY_STREET_AND_APT = "SELECT id FROM address WHERE city = ? and street = ? and apt = ? ";
    private static final String DELETE_ADDRESS_SQL = "DELETE FROM address WHERE id = ?";
    private static final String UPDATE_ADDRESS_SQL = "UPDATE address set city = ?,street = ?, apt= ? where id = ?;";

    public AddressDaoImpl() {
    }

    @Override
    public int save(Address address, Connection connection) throws SQLException {
        int index = -1;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ADDRESS_SQL, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, address.getCity());
        preparedStatement.setString(2, address.getStreet());
        preparedStatement.setInt(3, address.getApt());
        ResultSet rs = null;
        int rowAffected = preparedStatement.executeUpdate();
        if (rowAffected == 1) {
            // get candidate id
            rs = preparedStatement.getGeneratedKeys();
            if (rs.next())
                index = rs.getInt(1);
        }

        return index;
    }

    @Override
    public Address get(int id, Connection connection) throws SQLException {
        Address address = null;
        // Step 1: Establishing a Connection
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ADDRESS_BY_ID);
        preparedStatement.setInt(1, id);
        // Step 2: Execute the query or update query
        ResultSet rs = preparedStatement.executeQuery();

        // Step 3: Process the ResultSet object.
        while (rs.next()) {
            String city = rs.getString("city");
            String street = rs.getString("street");
            int apt = rs.getInt("apt");
            address = new Address(city, street, apt);
        }

        return address;
    }

    @Override
    public boolean update(Address address, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_ADDRESS_SQL);
        statement.setString(1, address.getCity());
        statement.setString(2, address.getStreet());
        statement.setInt(3, address.getApt());
        statement.setInt(4, address.getId());

        return statement.executeUpdate() > 0;
    }

    @Override
    public boolean delete(int id, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_ADDRESS_SQL);
        statement.setInt(1, id);

        return statement.executeUpdate() > 0;
    }

    @Override
    public int getByCityAndStreetAndApt(String city, String street, int apt, Connection connection) throws SQLException {
        // Step 1: Establishing a Connection
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_CITY_STREET_AND_APT);
        preparedStatement.setString(1, city);
        preparedStatement.setString(2, street);
        preparedStatement.setInt(3, apt);
        // Step 2: Execute the query or update query
        ResultSet rs = preparedStatement.executeQuery();
        int id = -1;
        // Step 3: Process the ResultSet object.
        while (rs.next()) {
            id = rs.getInt("id");
        }
        return id;
    }
}
