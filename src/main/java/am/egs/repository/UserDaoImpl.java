package am.egs.repository;

import am.egs.config.Hash;
import am.egs.entity.User;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    /*queries for sql*/
    private static final String INSERT_USERS_SQL = "INSERT INTO user (firstname, lastname,  email, password, education, telephone,is_admin, address_id) VALUES " +
            " (?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_USER_BY_ID = "select firstname, lastname, email, password, education, telephone,is_admin, address_id from user where id =?";
    private static final String SELECT_USER_BY_EMAIL = "select id, firstname, lastname, password, education, telephone, is_admin, address_id from user where email =?";
    private static final String SELECT_ALL_USERS = "select * from user";
    private static final String DELETE_USERS_SQL = "delete from user where id = ?;";
    private static final String SELECT_USERS_WITH_SAME_ADDRESS_SQL = "select * from user where address_id = ?;";
    private static final String UPDATE_USERS_SQL = "update user set firstname = ?,lastname = ?, email= ?, password =?, education = ?, telephone = ?, is_admin = ?, address_id = ? where id = ?;";

    public UserDaoImpl() {
    }

    @Override
    public int save(User user, Connection connection) throws SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        int index = -1;
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_SQL, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, user.getFirstName());
        preparedStatement.setString(2, user.getLastName());
        preparedStatement.setString(3, user.getEmail());
        preparedStatement.setString(4, Hash.hashPassword(user.getPassword()));
        preparedStatement.setString(5, user.getEducation());
        preparedStatement.setString(6, user.getTelephone());
        preparedStatement.setBoolean(7, user.isAdmin());
        preparedStatement.setInt(8, user.getAddressId());
        int rowAffected = preparedStatement.executeUpdate();
        if (rowAffected == 1) {
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                index = rs.getInt(1);
            }
        }
        return index;

    }

    @Override
    public User get(int id, Connection connection) throws SQLException {
        User user = null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID);
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            String education = rs.getString("education");
            String telephone = rs.getString("telephone");
            boolean isAdmin = rs.getBoolean("is_admin");
            int addressId = rs.getInt("address_id");
            user = new User(firstname, lastname, email, password, education, telephone, isAdmin, addressId);
        }
        return user;
    }

    @Override
    public User getByEmail(String email, Connection connection) throws SQLException {
        User user = null;
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_EMAIL);
        preparedStatement.setString(1, email);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("id");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String password = rs.getString("password");
            String education = rs.getString("education");
            String telephone = rs.getString("telephone");
            boolean isAdmin = rs.getBoolean("is_admin");
            int addressId = rs.getInt("address_id");
            user = new User(id, firstname, lastname, email, password, education, telephone, isAdmin, addressId);
        }
        return user;
    }

    @Override
    public List<User> getAll(Connection connection) throws SQLException {
        List<User> users = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            String education = rs.getString("education");
            String telephone = rs.getString("telephone");
            boolean isAdmin = rs.getBoolean("is_admin");
            int addressId = rs.getInt("address_id");
            users.add(new User(firstname, lastname, email, password, education, telephone, isAdmin, addressId));
        }

        return users;
    }

    @Override
    public boolean update(User user, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_SQL);
        statement.setString(1, user.getFirstName());
        statement.setString(2, user.getLastName());
        statement.setString(3, user.getEmail());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getEducation());
        statement.setString(6, user.getTelephone());
        statement.setBoolean(7, user.isAdmin());
        statement.setInt(8, user.getAddressId());
        statement.setInt(9, user.getId());

        return statement.executeUpdate() > 0;
    }

    @Override
    public boolean delete(int id, Connection connection) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_USERS_SQL);
        statement.setInt(1, id);

        return statement.executeUpdate() > 0;
    }

    public  List<User> getAllWithSameAddress(int id, Connection connection) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USERS_WITH_SAME_ADDRESS_SQL);
        List<User> users = new ArrayList<>();
        preparedStatement.setInt(1, id);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            int userid = rs.getInt("id");
            String firstname = rs.getString("firstname");
            String lastname = rs.getString("lastname");
            String email = rs.getString("email");
            String password = rs.getString("password");
            String education = rs.getString("education");
            String telephone = rs.getString("telephone");
            boolean isAdmin = rs.getBoolean("is_admin");
            users.add(new User(userid, firstname, lastname, email, password, education, telephone, isAdmin, id));
        }

        return users;
    }

}
