package am.egs.repository;

import am.egs.entity.Address;

import java.sql.Connection;
import java.sql.SQLException;

public interface AddressDao {
    Address get(int id, Connection connection) throws SQLException;

    int save(Address address, Connection connection) throws SQLException;

    boolean update(Address address, Connection connection) throws SQLException;

    boolean delete(int id, Connection connection) throws SQLException;

    int getByCityAndStreetAndApt(String city, String street, int apt, Connection connection) throws SQLException;
}
