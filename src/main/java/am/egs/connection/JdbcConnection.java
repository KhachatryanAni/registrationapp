package am.egs.connection;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JdbcConnection {

    private static DataSource dataSource;

    private JdbcConnection() {
    }

    private static DataSource getMysqlDataSourceConnection() {
        Properties props = loadProperties();

        MysqlDataSource ds = new MysqlDataSource();
        ds.setUrl(props.getProperty("db.conn.url"));
        ds.setUser(props.getProperty("db.username"));
        ds.setPassword(props.getProperty("db.password"));
        return ds;
    }

    private static Properties loadProperties() {
        Properties props = null;
        InputStream in = JdbcConnection.class.getClassLoader().getResourceAsStream("connection.properties");
        try {
            props = new Properties();
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            props.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return props;
    }

    public static DataSource getInstance() {
        if (dataSource == null) {
            dataSource = getMysqlDataSourceConnection();
            return dataSource;
        }
        return dataSource;
    }

}